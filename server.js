var express = require("express");
var app = express();
app.get("/users", (req, res, next) => {
    res.json(["Luke","Ivan","Michael","Ginger","Food"]);
});
app.listen(3000, () => {
    console.log("Server running on port 3000");
});
